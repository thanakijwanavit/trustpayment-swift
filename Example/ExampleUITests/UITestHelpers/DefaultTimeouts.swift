import Foundation

struct DefaultTestTimeouts {
    static let transactionResultAlertDisplayed: TimeInterval = 10
    static let threeDSecureFormDisplayed: TimeInterval = 6
    static let threeDSecureTextFieldDisplayed: TimeInterval = 10

    static let defaultSystemUiElementTimeout: TimeInterval = 5
}
