import XCTest

final class ApplePayActivityViewPage: BaseAppPage {
    // MARK: - Elements

    let springboard = XCUIApplication(bundleIdentifier: "com.apple.springboard")

    private var payWithPasscodeButton: XCUIElement {
        springboard.buttons["Pay with Passcode"]
    }

    private var passcodeOverlay: XCUIElement {
        springboard
    }

    private var cancelButton: XCUIElement {
        springboard.buttons["Cancel"]
    }

    // MARK: - Actions

    func tapPayWithPasscode() -> ApplePayActivityViewPage {
        payWithPasscodeButton.tap()
        return self
    }

    func typePasscode() {
        passcodeOverlay.buttons["1"].tap()
        passcodeOverlay.buttons["1"].tap()
        passcodeOverlay.buttons["0"].tap()
        passcodeOverlay.buttons["0"].tap()
    }

    func tapCancel() {
        cancelButton.waitForExistence(timeout: DefaultTestTimeouts.defaultSystemUiElementTimeout)
        cancelButton.tap()
    }
}
