//
//  ObjectiveCTest.h
//  Example
//

#import <Foundation/Foundation.h>

#ifdef CocoaPodsIntegration
@import TrustPayments;
#elif CarthageIntegration
@import TrustPaymentsCore;
@import TrustPaymentsUI;
@import TrustPayments3DSecure;
@import TrustPaymentsCard;
#endif

@protocol APIClient;

@interface ObjectiveCTest : NSObject <TPApplePayConfigurationHandler>

@property (nonatomic, strong, readwrite) PaymentTransactionManager *transactionManager;
@property (nonatomic, strong, readwrite) id<DropInController> dropInViewController;

- (void) testTransactionManager;
- (void) testDropInViewController;
- (void) testTranslations;
- (void) testApplePayConfig;
@end

