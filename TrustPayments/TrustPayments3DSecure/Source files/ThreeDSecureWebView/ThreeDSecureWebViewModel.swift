//
//  ThreeDSecureWebViewModel.swift
//  TrustPayments3DSecure
//

import WebKit

public final class ThreeDSecureWebViewModel {
    let payload: String
    let termUrl: String
    let acsUrl: String
    let mdValue: String
    let cardinalStyleManager: CardinalStyleManager?
    let cardinalDarkModeStyleManager: CardinalStyleManager?

    var webViewRequest: URLRequest {
        var components = URLComponents(string: acsUrl)!
        components.queryItems = [URLQueryItem(name: "PaReq", value: payload), URLQueryItem(name: "TermUrl", value: termUrl), URLQueryItem(name: "MD", value: mdValue)]

        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")

        let url = components.url!

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = ["Content-Type": "x-www-form-urlencoded"]
        return request
    }

    var webViewConfiguration: WKWebViewConfiguration {
        let preferences = WKPreferences()
        preferences.javaScriptCanOpenWindowsAutomatically = false
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        return configuration
    }

    public init(payload: String, termUrl: String, acsUrl: String, mdValue: String, cardinalStyleManager: CardinalStyleManager?, cardinalDarkModeStyleManager: CardinalStyleManager?) {
        self.payload = payload
        self.termUrl = termUrl
        self.acsUrl = acsUrl
        self.mdValue = mdValue
        self.cardinalStyleManager = cardinalStyleManager
        self.cardinalDarkModeStyleManager = cardinalDarkModeStyleManager
    }
}
