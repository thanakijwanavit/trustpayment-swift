//
//  ThreeDResponse.swift
//  TrustPaymentsCore
//

import Foundation

@objc public final class ThreeDResponse: NSObject {
    // property to store the threedsecure authentication value (3ds version 1)
    @objc public let pares: String?
    // property to store the threedsecure authentication value
    @objc public let threeDResponse: String?

    @objc public init(pares: String) {
        self.pares = pares
        threeDResponse = nil
    }

    @objc public init(threeDResponse: String) {
        pares = nil
        self.threeDResponse = threeDResponse
    }
}
