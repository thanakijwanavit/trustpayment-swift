//
//  JWTMerchantBodyResponse.swift
//  TrustPaymentsCore
//

struct JWTMerchantBodyPayload: Decodable {
    // MARK: Properties

    let typeDescriptions: [TypeDescription]
    let termUrl: String?
}

private extension JWTMerchantBodyPayload {
    enum CodingKeys: String, CodingKey {
        case typeDescriptions = "requesttypedescriptions"
        case termUrl = "termurl"
    }
}

struct JWTMerchantBodyResponse: APIResponse {
    // MARK: Properties

    let typeDescriptions: [TypeDescription]
    let termUrl: String?

    // MARK: Initialization

    /// - SeeAlso: Swift.Decodable
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let payload = try container.decode(JWTMerchantBodyPayload.self, forKey: .payload)
        typeDescriptions = payload.typeDescriptions
        termUrl = payload.termUrl
    }
}

private extension JWTMerchantBodyResponse {
    enum CodingKeys: String, CodingKey {
        case payload
    }
}
