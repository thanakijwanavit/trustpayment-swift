//
//  DecodedMerchantJWT.swift
//  TrustPaymentsCore
//

public class DecodedMerchantJWT: BaseDecodedJWT {
    // MARK: Properties

    private let jwtMerchantBodyResponse: JWTMerchantBodyResponse

    public var typeDescriptions: [TypeDescription] {
        jwtMerchantBodyResponse.typeDescriptions
    }

    public var termUrl: String? {
        jwtMerchantBodyResponse.termUrl
    }

    // MARK: Initialization

    /// Initializes an instance of the receiver.
    /// - Parameter jwt: encoded JWT token
    /// - Throws: error occurred when decoding the JWT
    override public init(jwt: String) throws {
        let parts = jwt.components(separatedBy: ".")
        guard parts.count == 3 else {
            throw APIClientError.jwtDecodingInvalidPartCount
        }
        jwtMerchantBodyResponse = try DecodedMerchantJWT.decodeJWTMerchantBodyByDecoder(parts[1])
        try super.init(jwt: jwt)
    }

    private static func decodeJWTMerchantBodyByDecoder(_ value: String) throws -> JWTMerchantBodyResponse {
        guard let bodyData = base64UrlDecode(value) else {
            throw APIClientError.jwtDecodingInvalidBase64Url
        }

        let decoder = JWTMerchantBodyResponse.decoder
        guard let payload = try? decoder.decode(JWTMerchantBodyResponse.self, from: bodyData) else {
            throw APIClientError.jwtDecodingInvalidJSON
        }

        return payload
    }
}
