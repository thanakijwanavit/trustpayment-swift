//
//  PayButtonStyleManager.swift
//  TrustPaymentsUI
//

import UIKit

@objc public class PayButtonStyleManager: RequestButtonStyleManager {
    /// Default appearance configuration of the Pay Button
    /// - Returns: Configured PayButtonStyleManager
    @objc public static func `default`() -> PayButtonStyleManager {
        PayButtonStyleManager(titleColor: .white,
                              enabledBackgroundColor: .black,
                              disabledBackgroundColor: UIColor.lightGray.withAlphaComponent(0.6),
                              borderColor: .clear,
                              titleFont: UIFont.systemFont(ofSize: 16, weight: .medium),
                              spinnerStyle: .white,
                              spinnerColor: .white,
                              buttonContentHeightMargins: HeightMargins(top: 15, bottom: 15),
                              borderWidth: 0,
                              cornerRadius: 6)
    }
}
