//
//  DropInViewModel.swift
//  TrustPaymentsUI
//

#if !COCOAPODS
    import TrustPayments3DSecure
    import TrustPaymentsCard
    import TrustPaymentsCore
#endif
import Foundation
import PassKit

final class DropInViewModel {
    // MARK: Properties

    private var paymentTransactionManager: PaymentTransactionManager

    private var jwt: String?

    private let visibleFields: [DropInViewVisibleFields]

    let applePayConfiguration: TPApplePayConfiguration?

    var transactionResponseClosure: (([String], ThreeDResponse?, APIClientError?) -> Void)?

    var isCardNumberFieldHidden: Bool {
        !visibleFields.contains(.pan)
    }

    var isCVVFieldHidden: Bool {
        !(visibleFields.contains(.cvv3) || visibleFields.contains(.cvv4))
    }

    var isExpiryDateFieldHidden: Bool {
        !visibleFields.contains(.expiryDate)
    }

    var isPayButtonHidden: Bool {
        visibleFields.isEmpty && applePayConfiguration?.isApplePayAvailable == true
    }

    var cardType: CardType {
        visibleFields.contains(.cvv4) ? .amex : .unknown
    }

    // MARK: Initialization

    /// Initializes an instance of the receiver.
    ///
    /// - Parameter jwt: jwt token
    /// - Parameter cardinalStyleManager: manager to set the interface style (view customization)
    /// - Parameter cardinalDarkModeStyleManager: manager to set the interface style in dark mode
    /// - Parameter visibleFields: specify which input fields should be visible
    /// - Parameter applePayConfiguration: Configuration of Apple Pay containing request and button styles. When requesttypedescriptions (JWT payload) parameter contains only THREEDQUERY, the apple pay settings will be omitted
    init(jwt: String?, applePayConfiguration: TPApplePayConfiguration?, cardinalStyleManager: CardinalStyleManager?, cardinalDarkModeStyleManager: CardinalStyleManager?, visibleFields: [DropInViewVisibleFields] = DropInViewVisibleFields.default) throws {
        self.jwt = jwt
        self.visibleFields = visibleFields

        // Omit the Apple Pay configuration if type desciptions parameter consist only of .threeDQuery
        if let jwt = jwt {
            let decodedJwt = try? DecodedMerchantJWT(jwt: jwt)
            self.applePayConfiguration = decodedJwt?.typeDescriptions == [TypeDescription.threeDQuery] ? nil : applePayConfiguration
        } else {
            self.applePayConfiguration = applePayConfiguration
        }

        paymentTransactionManager = try PaymentTransactionManager(jwt: jwt, cardinalStyleManager: cardinalStyleManager, cardinalDarkModeStyleManager: cardinalDarkModeStyleManager)

        self.applePayConfiguration?.proceedAfterApplePayAuthorization = { [weak self] jwt in
            self?.updateTokenAndPerformTransactionWithApplePay(jwt: jwt)
        }
    }

    /// executes payment transaction flow
    /// - Parameters:
    ///   - cardNumber: The long number printed on the front of the customer’s card.
    ///   - cvv: The three digit security code printed on the back of the card. (For AMEX cards, this is a 4 digit code found on the front of the card), This field is not strictly required.
    ///   - expiryDate: The expiry date printed on the card.
    func performTransaction(cardNumber: CardNumber, cvv: CVV?, expiryDate: ExpiryDate) {
        let card = Card(cardNumber: cardNumber, cvv: cvv, expiryDate: expiryDate)
        paymentTransactionManager.performTransaction(jwt: jwt, card: card, transactionResponseClosure: transactionResponseClosure)
    }

    /// Validates all input views in form
    /// - Parameter view: form view
    /// - Returns: result of validation
    @discardableResult
    func validateForm(view: DropInViewProtocol) -> Bool {
        // validate only fields that are added to the view's hierarchy
        let viewsToValidate = [view.cardNumberInput, view.expiryDateInput, view.cvvInput].filter { ($0 as? BaseView)?.isHidden == false }
        return viewsToValidate.count == viewsToValidate.filter { $0.validate(silent: false) }.count && view.additionalFieldsToValidate.count == view.additionalFieldsToValidate.filter { $0.validate(silent: false) }.count && view.isFormValid
    }

    // MARK: Helpers

    /// Updates JWT token
    /// - Parameter newValue: updated JWT token
    func updateJWT(newValue: String) {
        jwt = newValue
    }
}

// MARK: Apple Pay flow

extension DropInViewModel {
    /// Check if ApplePay is available and present payment controller
    func performApplePayAuthorization() {
        guard let topMostVC = UIApplication.shared.topMostViewController else { return }
        guard let request = applePayConfiguration?.request else { return }
        guard let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request) else { return }
        applePayController.delegate = applePayConfiguration
        topMostVC.present(applePayController, animated: true)
    }

    /// Executes payment transaction flow after authorization from Apple Pay
    func updateTokenAndPerformTransactionWithApplePay(jwt: String) {
        paymentTransactionManager.performWalletTransaction(walletSource: .applePay, jwt: jwt, transactionResponseClosure: transactionResponseClosure)
    }
}

/// Use to specify which card details fields should be visible.
///
/// Needed when all you need from your user is just to provide CVV code.
///
/// - warning: When providing both lengths of cvv, the one with 3 digits will be used.
@objc public enum DropInViewVisibleFields: Int {
    case pan = 0
    case expiryDate
    case cvv3
    case cvv4

    public static var `default`: [DropInViewVisibleFields] {
        [
            .pan,
            .expiryDate,
            .cvv3
        ]
    }
}
