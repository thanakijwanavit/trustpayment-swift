//
//  TestThreeDResponse.swift
//  TrustPaymentsCoreTests
//

@testable import TrustPaymentsCore
import XCTest

class TestThreeDResponse: XCTestCase {
    func test_pares() {
        let value = "pares"
        let threeDResponse = ThreeDResponse(pares: value)
        XCTAssertEqual(threeDResponse.pares, value)
        XCTAssertNil(threeDResponse.threeDResponse)
    }

    func test_threeDResponse() {
        let value = "threeDResponse"
        let threeDResponse = ThreeDResponse(threeDResponse: value)
        XCTAssertEqual(threeDResponse.threeDResponse, value)
        XCTAssertNil(threeDResponse.pares)
    }
}
