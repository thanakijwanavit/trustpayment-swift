fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios clearDerivedData
```
fastlane ios clearDerivedData
```
Clear Derived Data
### ios dependencies
```
fastlane ios dependencies
```
Install dependencies
### ios carthageIntegrationDependencies
```
fastlane ios carthageIntegrationDependencies
```
Install Carthage integration dependencies
### ios threeDModuleTests
```
fastlane ios threeDModuleTests
```
Run unit tests for the 3D Secure module
### ios cardModuleTests
```
fastlane ios cardModuleTests
```
Run unit tests for the Card module
### ios coreModuleTests
```
fastlane ios coreModuleTests
```
Run unit tests for the Core module
### ios uiModuleTests
```
fastlane ios uiModuleTests
```
Run unit tests for the UI module
### ios sslPinningTests
```
fastlane ios sslPinningTests
```
Run ssl pinning tests
### ios exampleAppUITests
```
fastlane ios exampleAppUITests
```
Run UI tests for the Example App
### ios exampleAppIntegrationTests
```
fastlane ios exampleAppIntegrationTests
```
Run integration & unit tests for the Example App
### ios exampleAppUnitLocaleTests
```
fastlane ios exampleAppUnitLocaleTests
```
Run unit tests(locale) for the Example App
### ios exampleAppSnapshotTests
```
fastlane ios exampleAppSnapshotTests
```
Run Snapshot test
### ios exampleAppIphone5sSmokeTests
```
fastlane ios exampleAppIphone5sSmokeTests
```
Run only E2E UI tests on iOS 12.4
### ios combinedCodeCoverage
```
fastlane ios combinedCodeCoverage
```
Collects code coverage for selected lanes, combines them display total code coverage for SDK modules
### ios carthageIntegrationTests
```
fastlane ios carthageIntegrationTests
```
Run selected UI and Unit tests at once for the Carthage Integration
### ios cocoaPodsIntegrationTests
```
fastlane ios cocoaPodsIntegrationTests
```
Run selected UI and Unit tests for the CocoaPods Integration
### ios runTests
```
fastlane ios runTests
```

### ios buildApp
```
fastlane ios buildApp
```
Build app for App Store or Ad Hoc
### ios prepareTestSuiteForBrowserStack
```
fastlane ios prepareTestSuiteForBrowserStack
```
Build the project to generate E2E test-suite for BrowserStack.
### ios exampleAppUITestsBrowserStack
```
fastlane ios exampleAppUITestsBrowserStack
```
Build the project to generate E2E test-suite for BrowserStack.
### ios upload
```
fastlane ios upload
```
Upload build to TestFlight

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
