// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "trustpayment-swift",
    platforms: [
            .iOS(.v13)
        ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "trustpayment-swift",
            targets: ["trustpayment-swift"]),
    ],
    dependencies: [
        .package(name: "SwiftJWT", url: "https://github.com/Kitura/Swift-JWT.git", from: "3.5.3"),
        .package(name: "KIF", url: "https://github.com/kif-framework/KIF.git", from: "3.7.11"),
        .package(name: "SnapshotTesting", url: "https://github.com/pointfreeco/swift-snapshot-testing.git", from: "1.8.12"),

        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        
        .target(
            name: "trustpayment-swift",
            dependencies: [
                "SwiftJWT",
                "KIF",
                "SnapshotTesting"
            ]),
        .testTarget(
            name: "trustpayment-swiftTests",
            dependencies: ["trustpayment-swift",
                           "SwiftJWT",
                           "KIF",
                           "SnapshotTesting"
                          ]),
    ]
)
