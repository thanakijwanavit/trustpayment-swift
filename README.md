# TrustPayments iOS SDK

[![Carthage compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage)
[![License MIT](https://img.shields.io/cocoapods/l/TrustKit.svg?style=flat)](https://en.wikipedia.org/wiki/MIT_License)



The Trust Payments iOS SDK allows you to accept card payments in your iOS app.

We provide a prebuilt and customisable UI which is SCA ready.

**The TrustPayments iOS SDK requires Xcode 10.2+**. It permits a Deployment Target of iOS 11.0 or higher.

## Supported Payment Methods

- Credit Cards
- ThreeDSecure

## Installation

We recommend using either [CocoaPods](https://github.com/CocoaPods/CocoaPods) or [Carthage](https://github.com/Carthage/Carthage) to integrate the TrustPayments SDK with your project.

#### CocoaPods
[CocoaPods](https://cocoapods.org) is a dependency manager for Cocoa projects. For usage and installation instructions, visit their website. To integrate Trust Payments into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
# Includes all modules
pod 'TrustPayments'

# Optionally, only the module of interest can be specified 
pod 'TrustPayments/Core' # Performing transactions using your own views
pod 'TrustPayments/UI' # Performing transactions by a ready to use “drop-in” controller
pod 'TrustPayments/Card' # Module with useful methods to validate card data
```

#### Carthage
[Carthage](https://github.com/Carthage/Carthage) is a decentralized dependency manager that builds your dependencies and provides you with binary frameworks. To integrate Trust Payments into your Xcode project using Carthage, specify it in your `Cartfile`:

```ogdl
git "https://gitlab.com/trustpayments-public/mobile-sdk/ios"
```
and [add the frameworks to your project](https://github.com/Carthage/Carthage#adding-frameworks-to-an-application).

## Documentation

#### SDK initialisation
Before you can perform any payment request, you have to set the gateway type, username and environment. Otherwise an error of type **TPInitError** will be thrown.

```swift
TrustPayments.instance.configure(username: appFoundation.keys.merchantUsername,
                                 gateway: .eu,
                                 environment: .staging,
                                 translationsForOverride: nil
)
```


#### “Drop-in” controller
The easiest way to use the full functionality of the SDK is to use **DropInViewController**.
It provides a fully fledged payments experience out of the box.

example of use:

```swift
let dropInVC = ViewControllerFactory.shared.dropInViewController(jwt: jwt, customDropInView: customDropInView, visibleFields: DropInViewVisibleFields.default, applePayConfiguration: nil, dropInViewStyleManager: dropInViewStyleManager, dropInViewDarkModeStyleManager: dropInViewDarkModeStyleManager, cardinalStyleManager: cardinalStyleManager, cardinalDarkModeStyleManager: cardinalDarkModeStyleManager, payButtonTappedClosureBeforeTransaction: { [unowned self] controller in
    // If this block is handled, further calls in the transaction flow are suspended, which gives the possibility to call asynchronous methods here, e.g. to update the JWT token. To resume operation, call continue() method.
    controller.continue()
}, transactionResponseClosure: { [unowned self] jwt, threeDResponse, error in
})

```
Important options:
- **customDropInView** - a property to embed your own form with custom components.
- **visibleFields** - parameter for hiding UI components. This is useful for tokenized payments, where only CVV is required from the user.
- **DropInViewStyleManager** - customisation of the form view. Support for both light and dark mode available.
- **CardinalStyleManager**  - customisation of the card issuers Access Control Server (ACS). Support for both light and dark mode available.


#### Payment transaction manager
It is also possible to carry out the entire transaction flow with views built by yourself using **PaymentTransactionManager**.

example of use:

```swift
let paymentTransactionManager = try PaymentTransactionManager(jwt: .empty)

paymentTransactionManager.performTransaction(jwt: jwt, card: card, transactionResponseClosure: { [unowned self] jwt, threeDResponse, error in
})

```

#### Locale and custom translations
The SDK supports several languages and offers the possibility of overwritten translations for a given language.

example of use:

```swift
TrustPayments.instance.configure(username: appFoundation.keys.merchantUsername,
                                 gateway: .eu,
                                 environment: .staging,
                                 translationsForOverride:
            [
                Locale(identifier: "fr_FR"):
                    [
                        LocalizableKeys.PayButton.title.key: "Payez maintenant!"
                ],
                Locale(identifier: "en_GB"):
                    [
                        LocalizableKeys.PayButton.title.key: "Pay Now!"
                ]
            ]
)
```

## Demo
Required tools: cocoapods, cocoapods-keys, carthage

A demo app is included in the project. To run it, run `pod install` (you will be asked by the cocoapods-keys for 3 keys - which will be delivered to you by Trust Payments), `carthage update` and then open `TrustPayments.xcworkspace` in Xcode.

In the prepared production application, the initialization keys should be stored in a safer way than through the cocoapods-key (we recommend storing in your private backend, only the user with the appropriate privileges can receive the key).

### License

The Trust Payments iOS SDK is open source and available under the MIT license. See the [LICENSE](LICENSE) file for more info.
